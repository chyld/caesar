package com.chyld.repositories;

import com.chyld.models.Album;
import com.chyld.models.Artist;
import com.chyld.models.Song;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ArtistRepository extends PagingAndSortingRepository<Artist, Integer> {
    public Artist findByName(String name);

    @Query("select sng from Album alb join alb.songs sng where alb.name = :name")
    public List<Song> findAllSongsByAlbumName(@Param("name") String name);

    @Query("select sng from Artist art join art.albums alb join alb.songs sng where art.name = :name and sng.isExplicit is true")
    public Page<Song> findAllSongsByArtistName(@Param("name") String name, Pageable pageable);
}
