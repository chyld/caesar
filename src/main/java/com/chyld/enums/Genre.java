package com.chyld.enums;

public enum Genre {
    ROCK, JAZZ, CLASSICAL, POP
}
