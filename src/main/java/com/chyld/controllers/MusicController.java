package com.chyld.controllers;

import com.chyld.models.Album;
import com.chyld.models.Artist;
import com.chyld.models.Song;
import com.chyld.repositories.AlbumRepository;
import com.chyld.repositories.ArtistRepository;
import com.chyld.repositories.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/music")
public class MusicController {

    @Autowired
    private AlbumRepository albums;

    @Autowired
    private SongRepository songs;

    @Autowired
    private ArtistRepository artists;

    @RequestMapping("/albums")
    public Iterable<Album> first(){
        return albums.findAll();
    }

    @RequestMapping("/songs")
    public Iterable<Song> second(){
        return songs.findAll();
    }

    @RequestMapping("/artists")
    public Iterable<Artist> third(){
        return artists.findAll();
    }

    @RequestMapping("/albums_from_kurt")
    public Iterable<Album> fourth(){
        return artists.findByName("kurt cobain").getAlbums();
    }

    @RequestMapping("/songs_from_thriller")
    public Iterable<Song> fifth(){
        return artists.findAllSongsByAlbumName("thriller");
    }

    @RequestMapping("/songs_from_mj")
    public Iterable<Song> sixth(){
        PageRequest pr = new PageRequest(0, 1);
        return artists.findAllSongsByArtistName("michael jackson", pr);
    }
}
