package com.chyld.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "songs")
@Data
public class Song {
    private int id;
    private String name;
    private int duration;
    private Album album;
    private boolean isExplicit;
    private Date published;

    @Id
    @GeneratedValue
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}

    @Column(name = "is_explicit", columnDefinition = "TINYINT")
    public boolean getIsExplicit() {return isExplicit;}
    public void setIsExplicit(boolean explicit) {isExplicit = explicit;}

    @Temporal(TemporalType.DATE)
    public Date getPublished() {return published;}
    public void setPublished(Date published) {this.published = published;}

    @ManyToOne
    @JoinColumn(name = "album_id")
    @JsonManagedReference
    public Album getAlbum() {return album;}
    public void setAlbum(Album album) {this.album = album;}
}
