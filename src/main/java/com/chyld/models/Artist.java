package com.chyld.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "artists")
@Data
public class Artist {
    private int id;
    private String name;
    private List<Album> albums;

    @Id
    @GeneratedValue
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}

    @OneToMany(mappedBy = "artist")
    @JsonBackReference
    public List<Album> getAlbums() {return albums;}
    public void setAlbums(List<Album> albums) {this.albums = albums;}
}
