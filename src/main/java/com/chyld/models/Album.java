package com.chyld.models;

import com.chyld.enums.Genre;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "albums")
@Data
public class Album {
    private int id;
    private String name;
    private Genre genre;
    private List<Song> songs;
    private Artist artist;

    @Id
    @GeneratedValue
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "enum('ROCK','JAZZ','CLASSICAL','POP')")
    public Genre getGenre() {return genre;}
    public void setGenre(Genre genre) {this.genre = genre;}

    @OneToMany(mappedBy = "album")
    @JsonBackReference
    public List<Song> getSongs() {return songs;}
    public void setSongs(List<Song> songs) {this.songs = songs;}

    @ManyToOne
    @JoinColumn(name = "artist_id")
    @JsonManagedReference
    public Artist getArtist() {return artist;}
    public void setArtist(Artist artist) {this.artist = artist;}
}
